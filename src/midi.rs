extern crate midir;

use std::error::Error;
use std::io::{stdin, stdout, Write};
use std::rc::Rc;
use async_channel::{Sender};
use futures::executor::block_on;

use crate::board;

use midir::{Ignore, MidiInput, MidiInputConnection};

enum MidiConnection {
    Input(MidiInputConnection<Sender<board::MidiInputMessage>>),
}

pub struct In {
    input: Vec<MidiConnection>,
    port: Rc<midir::MidiInputPort>,
}

impl In {
    pub async fn new(t: Sender<board::MidiInputMessage>) -> Result<In, Box<dyn Error>> {
        let mut midi_in = MidiInput::new("midir reading input")?;
        // Get an input port (read from console if multiple are available)
        let in_ports = midi_in.ports();
        let in_port = match in_ports.len() {
            0 => return Err("no input port found".into()),
            1 => {
                println!(
                    "Choosing the only available input port: {}",
                    midi_in.port_name(&in_ports[0]).unwrap()
                );
                in_ports[0].clone()
            }
            _ => {
                println!("\nAvailable input ports:");
                for (i, p) in in_ports.iter().enumerate() {
                    println!("{}: {}", i, midi_in.port_name(p).unwrap());
                }
                print!("Please select input port: ");
                stdout().flush()?;
                let mut ioinput = String::new();
                stdin().read_line(&mut ioinput)?;
                in_ports
                .get(ioinput.trim().parse::<usize>()?)
                .ok_or("invalid input port selected")?
                .clone()
            }
        };
        
        let mut inputs: In = In {
            input: Vec::new(),
            port: Rc::new(in_port),
        };
        midi_in.ignore(Ignore::None);
        
        let f =  move |_stamp: u64, message: &[u8], t: &mut Sender<board::MidiInputMessage>| {
            println!("Input changed {} {}", message[1], message[2]);
            block_on(t.send(board::MidiInputMessage {channel: message[1], value: message[2]})).expect("Can't send");
        };
        
        let _in_port_name = midi_in.port_name(&inputs.port)?;
        let conn_in = midi_in.connect(
            &inputs.port,
            "midir-read-input",
            f,
            t,
        )?;
        
        inputs.input.push(MidiConnection::Input(conn_in));
        
        Ok(inputs)
    }
}
