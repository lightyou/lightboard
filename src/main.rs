#[macro_use]
extern crate log;

mod board;

pub trait Output {
    fn render(&mut self, channel: usize, value: u8);
    fn board(&mut self, board: board::Board);
}

pub trait Input {
    fn value(&self) -> u8;
    fn channel(&self) -> u8;
}

mod dummy;
mod dmx;
mod midi;
mod fixture {
    
    //struct 
    pub struct Fixture {
        
    }
}

use async_channel::{unbounded};
use futures::select;
use futures::StreamExt;

#[async_std::main]
async fn main() {
    env_logger::init();
    
    // let (mut wsintx, wsinrx) = unbounded();
    // let (mut wsouttx, wsoutrx) = unbounded();
    // async_std::task::spawn(websocket::start_websocket(wsintx.clone(), wsoutrx.clone()));
    
    let mut board = board::Board::new().expect("Can't create board");
    board.associate(0, 0);
    board.associate(1, 10);
    board.associate(2, 20);
    board.associate(3, 21);
    board.associate(4, 22);
    board.associate(5, 23);
    board.associate(7, 25);
    
    
    let (intx, inrx) = unbounded();
    
    let dmxout = dummy::Out::new().expect("Failed to create DMX output");
    let wsout = websocket::InOut::new(intx.clone()).await.expect("Fail to create Websocket");
    
    let mut outputs = Vec::<Box<dyn Output>>::new();
    outputs.push(Box::new(dmxout));
    outputs.push(Box::new(wsout));
    
    let _midi = dummy::In::new(intx).await.expect("Error initializing midi");
    let mut rx_mut = inrx.clone();
    loop {
        println!("Enter loop");
        
        select! {        
            r = rx_mut.next() => {
                println!("Received Midi");
                match r.unwrap() {
                    board::Message::Input(r) => {
                        for out in outputs.iter_mut() {
                            receive_input(r, &mut board, Box::as_mut(out));
                        }
                    },
                    board::Message::Board(_) => {
                        
                    },
                    board::Message::Output(_) => {
                        
                    },
                    board::Message::Connect() => {
                        for out in outputs.iter_mut() {
                            out.board(board.clone());
                            for (channel, o) in board.matrix() {
                                out.render(*channel as usize, o.value);
                            }
                        }
                    }
                }
            },
        }
    }
}

mod websocket;

fn receive_input<I: Input, O: Output + ?Sized>(
    r: I,
    board: &mut board::Board,
    out: &mut O,
) {
    println!("Render {} {}", r.value(), r.channel());
    match board.from(&(r.channel() as u8)) {
        Some(value) => out.render(*value as usize, r.value()),
        None => println!("Unmatched input {}", r.value()),
    }
    board.set(r.channel(), r.value());
}

#[cfg(test)]
mod tests {
    use crate::board::MidiInputMessage;
    use async_channel::{unbounded};

    
    struct Test {
        value: u8,
        channel: usize
    }
    
    impl super::Output for Test {
        fn render(&mut self, channel: usize, value: u8) {
            self.value = value;
            self.channel = channel;
        }
        fn board(&mut self, _board: crate::board::Board) {
            
        }
    }

    impl super::Input for Test {
        fn value(&self) -> u8 {
            return self.value;
        }

        fn channel(&self) -> u8 {
            return self.channel as u8;
        }
    }

    #[test]
    fn test_1() {
        let mut o = Test {
            value: 0,
            channel: 0
        };
        let mut b = super::board::Board::new().unwrap();
        
        b.associate(10, 10);
        let x = MidiInputMessage {value: 20, channel: 10};
        crate::receive_input(x, &mut b, &mut o);
        assert!(o.channel == 10);
        assert!(o.value == 20);
        
        b.associate(15, 20);
        let x = MidiInputMessage {value: 30, channel: 15};
        crate::receive_input(x, &mut b, &mut o);
        assert!(o.channel == 20);
        assert!(o.value == 30);
        
    }

    #[test]
    fn test_2() {
        let mut board = crate::board::Board::new().expect("Can't create board");
        board.associate(0, 0);
        board.associate(1, 10);

        let (intx, inrx) = unbounded();

        let r = inrx.recv();
        match r.next().unwrap() {
            board::Message::Input(r) => {
            },
            board::Message::Board(_) => {
                
            },
            board::Message::Output(_) => {
                
            },
            board::Message::Connect() => {
            }
        }
    }
    
}
    