use std::println;
use std::sync::Arc;

use async_std::net::{SocketAddr, TcpListener, TcpStream};
use async_channel::{unbounded};
use async_channel::{Sender, Receiver};

use async_std::sync::Mutex;
use async_tungstenite::{
    accept_async,
    tungstenite::{Error, Result, Message},
};

use futures::prelude::*;
use futures::select;    
use futures::executor::block_on;

use crate::board;

#[derive(Clone)]
pub struct InOut {
    outchannel: Arc<Mutex<Vec<Sender<String>>>>,
}
impl InOut {
    pub async fn new(t: Sender<board::Message>) -> Result<InOut, Box<dyn std::error::Error>> {
        let inout = InOut { 
            outchannel: Arc::new(Mutex::new(Vec::new())),
        };
        async_std::task::spawn(start_websocket(t.clone(), inout.outchannel.clone()));
        
        Ok(inout)
    }
}

pub async fn start_websocket(t: Sender<board::Message>, inout: Arc<Mutex<Vec<Sender<String>>>>) {
    println!("Start websocket");
    let server = TcpListener::bind("0.0.0.0:9001").await.unwrap();
    println!("Hello {:?}", server);
    
    let mut incoming = server.incoming();
    
    while let Some(Ok(stream)) = incoming.next().await {
        println!("Connection accepted");
        
        let peer = stream
        .peer_addr()
        .expect("connected streams should have a peer address");
        info!("Peer address: {}", peer);

        let (outtx, outrx) = unbounded();
        inout.lock().await.push(outtx);
        
        async_std::task::spawn(accept_connection(peer, stream, t.clone(), outrx));
        println!("Wait for next incoming ws");
    };
}

async fn accept_connection(peer: SocketAddr, stream: TcpStream, t: Sender<board::Message>, out: Receiver<String>) {
    println!("accept_connection");
    if let Err(e) = handle_connection(peer, stream, t, out).await {
        match e {
            Error::ConnectionClosed | Error::Protocol(_) | Error::Utf8 => (),
            err => error!("Error processing connection: {}", err),
        }
    }
}

async fn handle_connection(peer: SocketAddr, stream: TcpStream, t: Sender<board::Message>, mut outrx: Receiver<String>) -> Result<()> {
    let ws_stream = accept_async(stream).await?;
    
    println!("New WebSocket connection: {}", peer);


    let (mut ws_sender, ws_receiver) = ws_stream.split();
    let mut ws_rec_fused = ws_receiver.fuse();
    t.send(board::Message::Connect()).await.unwrap();
    
    loop {
        select! {
            ws = ws_rec_fused.next() => {
                match ws {
                    Some(msg) => {
                        println!("Receive ws {:#?}", msg);
                        match msg {
                            Ok(Message::Close(_)) => break,
                            Ok(Message::Text(text)) => {
                                match serde_json::from_str::<board::Message>(&text) {
                                    Ok(message) => {
                                        t.send(message).await.expect("Can't send to channel for websocket");
                                    },
                                    Err(e) => println!("{} is not well formed json: {}", text, e) 
                                }
                            },
                            Ok(Message::Binary(_)) =>(),
                            Ok(Message::Ping(payload)) => {
                                ws_sender.send(Message::Pong(payload)).await.unwrap();
                            },
                            Ok(Message::Pong(_)) =>(),
                            Ok(Message::Frame(_)) =>(),
                            Err(err) => {
                                match err {
                                    Error::Io(_) => (),
                                    Error::ConnectionClosed => (),
                                    Error::AlreadyClosed => (),
                                    Error::Tls(_) => (),
                                    Error::Capacity(_) => (),
                                    Error::Protocol(_) => (),
                                    Error::SendQueueFull(_) => (),
                                    Error::Utf8 => (),
                                    Error::Url(_) => (),
                                    Error::Http(_) => (),
                                    Error::HttpFormat(_) => (),
                                }
                                println!("Error receiving message {:?}", err);
                                break;
                            },
                        };
                        ()
                    },
                    None => (),
                }
            },
            ch = outrx.next() => {
                println!("Receive data to output ws");
                ws_sender
                    .send(Message::text(ch.unwrap())).await
                    .unwrap();
                    ()
                }
        }
    }
    println!("Connection closed");
    
    ws_sender.close().await?;
    outrx.close();
    
    Ok(())
}

impl super::Output for InOut {
    fn render(&mut self, channel: usize, value: u8) {
        let out = serde_json::to_string(&board::Message::Output(board::DMXOutputMessage{channel: channel.try_into().unwrap(), value})).unwrap();
        for o in block_on(self.outchannel.lock()).iter() {
            println!("Render WS : {}", out.clone());
            let _ = block_on(o.send(out.clone()));
        }
    }
    fn board(&mut self, board: board::Board) {
        let out = serde_json::to_string(&board::Message::Board(board)).unwrap();
        // println!("outchannel {:?}", self.outchannel.clone());
        for o in block_on(self.outchannel.lock()).iter() {
            println!("Send board to WS : {}", out.clone());
            let _ = block_on(o.send(out.clone()));
        }
    }
}
