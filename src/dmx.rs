use enttecopendmx;
// use libftd2xx::FtStatus;
use std::error::Error;

pub struct Out {
    pub interface: enttecopendmx::EnttecOpenDMX,
}

unsafe impl Send for Out {}

impl Out {
    pub fn new() -> Result<Out, Box<dyn Error>> {
        let mut dmxout: Out = Out {
            interface: enttecopendmx::EnttecOpenDMX::new()?,
        };
        dmxout.interface.open()?;
        
        Ok(dmxout)
    }
}

impl super::Output for Out {
    fn render(&mut self, channel: usize, value: u8) {
        self.interface.set_channel(channel, value);
        self.interface.render().expect("Can't render");
        info!("Out {}: {}", channel, value);
    }
    fn board(&mut self, _board: crate::board::Board) {
        
    }
}