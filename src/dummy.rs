use std::error::Error;
use std::io::{stdin, stdout, Write};
use async_std::channel::Sender;
use async_std::task;
use crate::board::{Message, MidiInputMessage};

pub struct In {}
impl In {
    pub async fn new(t: Sender<Message>) -> Result<In, Box<dyn Error>> {
        let input = In {};
        task::spawn(async move { loop {
            print!("Please select input port and value: ");
            stdout().flush().expect("Error");
            let mut ioinput = String::new();
            stdin().read_line(&mut ioinput).expect("Error");
            let channel = ioinput
            .trim()
            .parse::<u8>()
            .expect("Enter a valid usize");
            let mut ioinput = String::new();
            stdin().read_line(&mut ioinput).expect("Error");
            let value = ioinput.trim().parse::<u8>().expect("Enter a value u8");
            t.send(Message::Input(MidiInputMessage{value, channel})).await.expect("Error sending message");
        }});
        Ok(input)
    }
}

pub struct Out {}
impl Out {
    pub fn new() -> Result<Out, Box<dyn Error>> {
        Ok(Out {})
    }
}

impl super::Output for &Out {
    fn render(&mut self, channel: usize, value: u8) {
        println!("Out {}: {}", channel, value);
    }
    fn board(&mut self, _board: crate::board::Board) {
        
    }
}

impl super::Output for Out {
    fn render(&mut self, channel: usize, value: u8) {
        println!("Out {}: {}", channel, value);
    }
    fn board(&mut self, _board: crate::board::Board) {
        
    }
}