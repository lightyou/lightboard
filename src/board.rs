use serde::{Deserialize, Serialize};
use std::{collections::{HashMap, hash_map::Iter}, iter::Map};

use crate::{Input, Output};

#[derive(Serialize, Deserialize, Clone, Debug, Hash, PartialEq, Eq)]
pub enum BoardType {
    DMX,
    Midi,
    Audio,
    Websocket,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct OutputPatch {
    pub output_type: BoardType, 
    pub channel: u8,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct OutputMessage {
    pub channel: u8,
    pub value: u8,
}

// [Midi 1, WS 1] (10) -> [DMX 1, WS 1]
// [Midi 2, WS 2] (255) -> [DMX 2, WS 2]
// [Midi 3, WS 3] (0) -> [Audio 1, WS 3]
// [Midi 4, WS 4] (22) -> [DMX 3, WS 4]
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct InputPatch {
   pub input_type: BoardType,
   pub channel: u8, 
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct Patch {
    pub input_list: Vec<InputPatch>,
    pub output_list: Vec<OutputPatch>,
    pub value: u8,
}


#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Board {
    matrix:  HashMap<u8, OutputMessage>,
    #[serde(skip)]
    patchs: Vec<Patch>,
}

#[derive(Debug)]
pub enum Error {
    Overflow,
}

impl Board {
    pub fn new() -> Result<Board, Error> {
        Ok(Board {
            matrix: HashMap::new(),
            patchs: Vec::new(),
        })
    }
    pub fn associate(&mut self, input: u8, channel: u8) {
        self.matrix.insert(input, OutputMessage {channel, value: 0});
    }
    
    pub fn patch(&mut self, input: Vec<InputPatch>, output: Vec<OutputPatch>) {
        self.patchs.push(Patch {
            input_list: input,
            output_list: output,
            value: 0,
        })
    }

    pub fn from(&self, input: &u8) -> Option<&u8> {
        Some(&self.matrix.get(input)?.channel)
    }

    pub fn matrix(&self) -> Iter<u8, OutputMessage> {
        return self.matrix.iter();
    }
    pub fn set(&mut self, channel: u8, value: u8) {
        self.matrix.get_mut(&channel).unwrap().value = value;
    }
}


#[derive(Clone, Serialize, Deserialize)]
pub enum Message {
    Input(MidiInputMessage),
    Output(DMXOutputMessage),
    Board(Board),
    Connect(),
}

#[derive(Clone, Copy, Serialize, Deserialize)]
pub struct DMXOutputMessage {
    pub value: u8,
    pub channel: u8
}
#[derive(Clone, Copy, Serialize, Deserialize)]
pub struct MidiInputMessage {
    pub value: u8,
    pub channel: u8
}

impl Input for MidiInputMessage {
    fn value(&self) -> u8 {
        self.value
    }
    fn channel(&self) -> u8 {
        self.channel
    }
}
