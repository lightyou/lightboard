/**
 * @license
 * Copyright 2018 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
import path from 'path';
import summary from 'rollup-plugin-summary';
// import {terser} from 'rollup-plugin-terser';
import resolve from '@rollup/plugin-node-resolve';
import replace from '@rollup/plugin-replace';
import webWorkerLoader from 'rollup-plugin-web-worker-loader';
import typescript from 'rollup-plugin-typescript2';
import {liveServer} from 'rollup-plugin-live-server';
import commonjs from 'rollup-plugin-commonjs';
import sourceMaps from 'rollup-plugin-sourcemaps';
import pkg from './package.json';


const extensions = [
  '.js', '.jsx', '.ts', '.tsx',
];

const moduleName = pkg.name.split('/').pop().replace(/-/g, '');
const config = [];

if (process.env.TARGET === 'debug') {
  config.push({
    input: [path.resolve(__dirname, pkg.entry)],
    output: {
        file: path.resolve(__dirname, pkg.iife),
        format: 'iife',
        name: moduleName,
        sourcemap: true,
    },
    // onwarn(warning) {
    //   if (warning.code !== 'THIS_IS_UNDEFINED') {
    //     console.error(`(!) ${warning.message}`);
    //   }
    // },
    plugins: [
      replace({
        'Reflect.decorate': 'undefined',
        "preventAssignment": true,
        "process.env.NODE_ENV": JSON.stringify("development")
      }),
      resolve({ extensions }),
      commonjs(),
      webWorkerLoader(/* configuration */),
      // terser({
      //   ecma: 2017,
      //   module: true,
      //   warnings: true,
      //   mangle: {
      //     properties: {
      //       regex: /^__/,
      //     },
      //   },
      // }),
      summary(),
      typescript({
        typescript: require('typescript'),
        cacheRoot: path.resolve(__dirname, '.rts2_cache'),
      }),
      liveServer({
        port: 8090,
        host: '0.0.0.0',
        root: 'www',
        file: 'index.html',
        mount: [['/dist/iife', './dist/iife']],
        open: false,
        logLevel: 2,
        wait: 500,
      }),
    ],
  });
} else {
  /* ESNext */
  config.push({
      input: [path.resolve(__dirname, pkg.entry)],
      output: {
          file: path.resolve(__dirname, pkg['module']),
          format: 'esm',
          sourcemap: true,
      },
      plugins: [
          replace({'Reflect.decorate': 'undefined'}),
          resolve({ extensions }),
          commonjs(),
          webWorkerLoader(/* configuration */),
          // summary(),
          typescript({
              typescript: require('typescript'),
              cacheRoot: path.resolve(__dirname, '.rts2_cache'),
              clean: true,
          }),
          sourceMaps(),
      ],
  });
}

module.exports = config;
