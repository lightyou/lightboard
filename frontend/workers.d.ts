declare module 'web-worker:*' {
    const WorkerFactory: new () => Worker;
    export default WorkerFactory;
}

declare module 'shared-worker:*' {
    const SWorkerFactory: new () => SharedWorker;
    export default SWorkerFactory;
}


declare module 'shared-worker:*' {
    const IWorkerFactory: new () => ServiceWorker;
    export default IWorkerFactory;
}
