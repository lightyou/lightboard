/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */


import {LitElement, html, css} from 'lit';
import {live} from 'lit/directives/live.js';
import {customElement, property} from 'lit/decorators.js';

/**
 * An example element.
 *
 * @fires count-changed - Indicates when the count changes
 * @slot - This element has a slot
 * @csspart button - The button
 */
@customElement('light-faders')
export class LightFaders extends LitElement {
  static override styles = css`
    :host {
      /* display: inline-block;
      position: relative; */
      --width: 16px;
      --height: 230px;

      /* Native vertical sliders have increased rightward margin. */
      /* margin-right: calc(var(--width) / 2); */
    }
    input[type=range] {
      display: inline-flex;
      /* width and height get swapped due to how the transform
      gets calculated, they will get reversed when turned vertical */
      width: var(--height);
      height: var(--width);
      left: 0;

      /* Pushes the slider slightly upward off the bottom of the line */
      bottom: -0.75em;

      /* Rotation -90deg makes sliding upward increase, and
      downward decrease. TranslateY centers us since we're
      absolutely positioned */
      transform: rotate(-90deg) translateY(calc(var(--width) / 2));
      transform-origin: left;
      position: absolute;

      -webkit-appearance: none;
      appearance: none;
      background: transparent;
      cursor: pointer;
      width: 15rem;
    }
    /***** Track Styles *****/
    /***** Chrome, Safari, Opera, and Edge Chromium *****/
    input[type="range"]::-webkit-slider-runnable-track {
      background: #053a5f;
      height: 0.5rem;
    }

    /******** Firefox ********/
    input[type="range"]::-moz-range-track {
      background: #053a5f;
      height: 0.5rem;
    }
    /***** Thumb Styles *****/
    /***** Chrome, Safari, Opera, and Edge Chromium *****/
    input[type="range"]::-webkit-slider-thumb {
      -webkit-appearance: none; /* Override default look */
      appearance: none;
      margin-top: -12px; /* Centers thumb on the track */
      background-color: #5cd5eb;
      height: 2rem;
      width: 1rem;    
    }
    /***** Thumb Styles *****/
    /***** Firefox *****/
    input[type="range"]::-moz-range-thumb {
        border: none; /*Removes extra border that FF applies*/
        border-radius: 0; /*Removes default border-radius that FF applies*/
        background-color: #5cd5eb;
        height: 2rem;
        width: 1rem;
    }
    .faderContainer {
      display: flex;
      width: var(--width);
      height: var(--height);
      position: relative;

      /* center the slider */
      /* margin: 0 auto; */
      margin: 15px;
    }
    label {
      display: inline-flex;
    }
    .faders {
      display: flex;
      height: 220px;
      /* flex-direction: row; */
      text-align: center;
      /* align-items: center; */
      /* justify-content: center; */
    }
  `;
  
  /**
   * Web Worker proporty
  */
 @property({type: Worker}) private worker: Worker | null = null;

  private _matrix: {[input: string]: {[output: string]: number}};
  private _values: {[channel: number]: number};
  private _onChanges: {[channel: number]: any};

  constructor() {
    super();
    this._values = {};
    this._matrix = {};
    this._onChanges = {};
    console.log(this.worker);
  }
  
  override connectedCallback() {
    super.connectedCallback();
  }
  
  async firstUpdated() {
    this.worker?.addEventListener('error', (e) => {
      console.log("Worker Error : ",e);
    });
    this.worker?.addEventListener('messageerror', (e) => {
      console.log("Worker Message Error : ",e);
    });
    this.worker?.addEventListener('message', (e) => {
      let message = JSON.parse(e.data);
      if ('Board' in message) {
        this._matrix = message.Board.matrix;
        console.log(this._matrix);
        for (let input in this._matrix) {
          let value = this._matrix[input]["value"];
          this._values[Number(input)] = value;
          this._onChanges[Number(input)] = () => {
            let elm: HTMLInputElement | null | undefined = 
            this.shadowRoot?.querySelector(`#fader_${input}`);
            const value = elm?.value;
            console.log("onChange", value);
            this.worker?.postMessage({'channel': Number(input), 'value': Number(value)});
          }
        }
      }
      else {
        let channel : string = "";
        for (let input in this._matrix) {
          let output = this._matrix[input]["channel"];
          if (output === message.Output.channel) {
            channel = input.toString();
          } 
        }
        console.log("rec", message, channel, message.Output.value);
        this._values[Number(channel)] = message.Output.value;
      }
      console.log("Request Update");
      this.requestUpdate();
    })
    // console.log("W ", this.worker);
  }

  override render() {
    let faders = [];
    for (let input in this._matrix) {
      let output = this._matrix[input]["channel"];
      let fader = html`<div>
        <div for="value">${this._values[Number(input)]}</div>
        <div class="faderContainer">
        <input id=${"fader_" + input} @input="${this._onChanges[Number(input)]}" type="range" name="value"
        min="0" max="255" .value="${live(this._values[Number(input)].toString())}" title="fader" /></div>
        <label for="value">${input}</label><br/>
        <label for="value">${output}</label>
      </div>`
      faders.push(fader);
    }
    return html`
      <div class="faders">
        ${faders}
      </div>
      <slot></slot>
    `;
  }

}

