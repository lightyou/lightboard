import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
/**
 *
 */
@customElement('light-board')
export class LightBoard extends LitElement {
  static override styles = css`
    :host {
    }
  `;

  /**
   * Web Worker proporty
  */
  @property({type: Worker}) private worker: Worker | null = null;

  private _matrix: {[input: string]: {[output: string]: number}};
  private _onChangeInput: {[channel: number]: any};

  constructor() {
    super();
    this._matrix = {};
    this._onChangeInput = {};
  }

  override connectedCallback() {
    super.connectedCallback();
  }

  async firstUpdated() {
    this.worker?.addEventListener('message', (e) => {
      let message = JSON.parse(e.data);
      if ('Board' in message) {
        this._matrix = message.Board.matrix;
        console.log(this._matrix);
        for (let input in this._matrix) {
          this._onChangeInput[Number(input)] = () => {
            let elmIn: HTMLInputElement | null | undefined = 
              this.shadowRoot?.querySelector(`#input_${input}`);
            let elmOut: HTMLInputElement | null | undefined = 
              this.shadowRoot?.querySelector(`#output_${input}`);
            const valueIn = elmIn?.value;
            const valueOut = elmOut?.value;
            if (valueIn !== undefined && valueOut !== undefined) {
              this._matrix[Number(valueIn)]["channel"] = Number(valueOut);
            }
            this.worker?.postMessage({'Board': {'matrix': this._matrix}});
          }
        }

      }
      else {
        console.log(message);
      }
      console.log("Request Update");
      this.requestUpdate();
    })
  }

  override render() {
    let rows = [];
    for (let input in this._matrix) {
      rows.push(html`<tr><td>
        <input id=${"input_"+input} value=${input} type="text" @change=${this._onChangeInput[Number(input)]}/>
        </td><td>
        <input id=${"output_"+input} value=${this._matrix[input]["channel"]} type="text" @change=${this._onChangeInput[Number(input)]}/>
        </td></tr>`)
    }
    return html`
      <table>
        <thead><tr>
          <th>Input</th>
          <th>Output</th>
        </tr></thead>
        ${rows}
      </table>
      <slot></slot>
    `;
  }

}

