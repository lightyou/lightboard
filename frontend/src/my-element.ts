import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import WebWorker from 'web-worker:./shared-worker.ts';
import './light-faders';
import './light-board';
/**
 *
 */
@customElement('my-element')
export class MyElement extends LitElement {
  static override styles = css`
    :host {
      display: flex;
    }
    table {
      margin: 10px;
      font-family: 'Roboto';
    }
  `;

  @property({type: Worker})
  private myWorker;
  

  constructor() {
    super();
    
    this.myWorker = new WebWorker();
    this.myWorker.onerror = () => console.log("Error from worker");
  }

  override connectedCallback() {
    super.connectedCallback();
  }

  async firstUpdated() {

  }

  override render() {
    return html`
      <light-faders .worker=${this.myWorker}></light-faders>
      <light-board .worker=${this.myWorker}></light-board>
      <slot></slot>
    `;
  }

}

