// shared-worker.ts
// This is the shared worker script that runs in the background
// and handles the websocket connection

/**
 * NOTE:
 *
 * Fix for typescript complaints during bundling:
 *   semantic error TS2304: Cannot find name 'SharedWorkerGlobalScope'
 */
// @ts-ignore
const worker = (self as unknown) as SharedWorkerGlobalScope;
let ws: WebSocket | undefined = undefined;

let values: { [channel: number]: number } = {};
function connect() {
    // Create a new websocket instance
    let timer: NodeJS.Timeout;
    try {

        if (ws === undefined) {
            console.log(ws);
            ws = new WebSocket("ws://localhost:9001");
        }
        
        // for (let channel in values) {
        //     ws.send(JSON.stringify({'Input': { 'value': values[channel], 'channel': channel }}));
        // }
        function listener_port(e: MessageEvent) {
            const message = e.data || e;
            if ('Board' in message) {
                ws?.send(JSON.stringify(message));
            }
            else {
                values[message.channel] = message.value;
                ws?.send(JSON.stringify({'Input': { 'value': message.value, 'channel': message.channel }}));
            }
            
        }

        ws.addEventListener('open', e => {
            console.log("Websocket connected : ", e)
            clearInterval(timer);
            worker.removeEventListener('message', listener_port);
            ws?.addEventListener('message', (e) => {
                const message = e.data || e;
                postMessage(message);
                console.log("Reçu", message);
            });
        })
        ws.addEventListener('error', e => {
            console.error("Websocket error : ", e)
            clearInterval(timer);
            worker.removeEventListener('message', listener_port);
        })
        ws.addEventListener('close', e => {
            console.info("Close : ", e.reason);
            clearInterval(timer);
            worker.onmessage = undefined;
            ws?.close();
            timer = setTimeout(function () {
                connect();
            }, 5000);
        })
        // console.log("Add event listener");
        worker.onmessage =  listener_port;
        worker.addEventListener('error', (e: ErrorEvent) => {
            console.error("Shared-Worker error : ", e);
            ws?.close();
            ws = undefined;
        });
    } catch {
        timer = setTimeout(function () {
            connect();
        }, 5000);

    }
}

connect();
